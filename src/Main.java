import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.stream;

interface repository {
    void print();
}

class ARepository implements repository {
    @Override
    public void print() {
        System.out.println("I'm ARepository");
    }
}

class BService {
    final repository myARepository;

    BService(repository myARepository) {
        this.myARepository = myARepository;
    }

    void print() {
        myARepository.print();
        System.out.println("I'm BService");
    }
}

public class Main {

    static Map<Class<?>, Object> myClass = new HashMap<>();

    public static void main(String[] args) {
        registerClass(ARepository.class);
        registerClass(BService.class);
        getBean(BService.class).print();
    }

    private static <T> T getBean(Class<T> bClass) {
        return (T) myClass.get(bClass);
    }

    private static void registerClass(Class<?> clazz) {
        final Constructor<?> constructor = clazz.getDeclaredConstructors()[0];
        final Class<?>[] parameterTypes = constructor.getParameterTypes();
        if (parameterTypes.length == 0) {
            saveTheClass(clazz, constructor);
        } else {
            saveTheClass(clazz, constructor, stream(parameterTypes).map(Main::getBean).toArray());
        }
    }

    private static void saveTheClass(Class<?> clazz, Constructor<?> constructor, Object... params) {
        try {
            final Object instance = constructor.newInstance(params);
            myClass.put(clazz, instance);
            stream(clazz.getInterfaces()).forEach(indicator ->
                myClass.put(indicator, instance)
            );
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            System.out.println(String.format("Wrong configuration %s", e));
        }
    }
}
